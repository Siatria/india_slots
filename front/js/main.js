let searchBtn = document.querySelector('.slots-page__filter-search .search-btn')
let slotsSearch = document.querySelector('.slots-page__filter-search .slots-search')
let closeBtn = document.querySelector('.slots-page__filter-search .close-btn')
let submitBtn = document.querySelector('.slots-page__filter-search .btn-submit')

searchBtn.addEventListener('click', () => {
    searchBtn.style.display = "none";
    slotsSearch.style.display = "block";
    closeBtn.style.display = "block";
    submitBtn.style.display = "none";
});

closeBtn.addEventListener('click', () => {
    searchBtn.style.display = "flex";
    slotsSearch.style.display = "none";
    closeBtn.style.display = "none";
});

let slotsAll = document.querySelector('.slots-page__all')
let slotsMore = document.querySelector('.slots-page__more')

slotsMore.addEventListener('click', () => {
    slotsAll.classList.toggle('toggle-list');
    slotsMore.style.display = "none";
});